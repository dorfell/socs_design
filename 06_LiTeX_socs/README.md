-------------------------------------------------------------------

     =========================================================
     socs_design/06_LiTeX_socs - Design of SoCs in LiTex
     =========================================================

                         List of modules
                         ---------------

SoCs designs from the fpga_101 for LiTeX designed with Python and implemented in the
Zybo-Z7 development board.


lab003
 - SoC composed by an uart brige manage with the litex server  and some peripherals
   (e.g. leds, switches and pushbuttons).

lab004
 - SoC composed by a softcore processor (e.g. picorv32, vexrisc) a wishbone
   communication bus and peripherals (e.g. leds, switches, etc.).

README.md
 - You're reading it righ now  8-P. 
