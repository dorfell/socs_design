-- SoC inferred rom 
-- (c) Dorfell Parra  <dlparrap@unal.edu.co>


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use std.textio.all;
use ieee.std_logic_textio.all;


entity inferred_rom is
  generic(
    -- Specify ROM data width
    C_ROM_WIDTH : integer :=   32; 
    -- Specify ROM depth (number of entries)
    C_ROM_DEPTH : integer := 4096; 
    -- Specify name/location of ROM application file 
    C_INIT_FILE : string := "../../../software/00_uart/app.hex"
  );
  port(
    clka  :  in std_logic;
    addra :  in std_logic_VECTOR(11 downto 0);
    douta : out std_logic_VECTOR(31 downto 0)
  );
end inferred_rom;

architecture behavioral of inferred_rom is


-- 2D Array Declaration for ROM signal
type rom_type is array (0 to C_ROM_DEPTH-1) of std_logic_vector (C_ROM_WIDTH-1 downto 0);


-- The folowing code initializes the memory values to a specified file to match hardware
impure function InitRomFromFile (rom_file_name : in string) return rom_type is
  file     rom_file  : text is in rom_file_name;
  variable file_line : line;
  variable rom       : rom_type;
  
  begin
    for i in rom_type'range loop
      if(not endfile(rom_file)) then
        readline(rom_file, file_line);
        hread(file_line, rom(i));
      else 
        rom(i) := (others => '0');
      end if;
    end loop;
  return rom;
end function;

-- Define ROM
signal rom       : rom_type := InitRomFromFile(C_INIT_FILE);
signal rom_data  : std_logic_vector(C_ROM_WIDTH-1 downto 0) := (others => '0');
signal douta_reg : std_logic_vector(C_ROM_WIDTH-1 downto 0) := (others => '0'); 


begin


-- Reads the memory address  per clock edge
process(clka)
  begin
    if(clka'event and clka = '1') then
      rom_data <= rom(conv_integer(addra));
    end if;
end process;

douta <= rom_data;

end behavioral;
