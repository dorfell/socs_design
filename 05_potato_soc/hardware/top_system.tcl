# *.tcl for creating the FPGA configuration file
# (c) Dorfell Parra  <dlparrap@unal.edu.co>


# Read the *.vhd sources
read_vhdl hardware/rtl/top_system.vhd
read_vhdl hardware/rtl/clock_generator.vhd
read_vhdl hardware/rtl/pp_soc_reset.vhd
read_vhdl hardware/rtl/packages/pp_constants.vhd
read_vhdl hardware/rtl/packages/pp_types.vhd
read_vhdl hardware/rtl/packages/pp_utilities.vhd
read_vhdl hardware/rtl/peripherals/aee_rom_wrapper.vhd
read_vhdl hardware/rtl/peripherals/inferred_rom.vhd
read_vhdl hardware/rtl/peripherals/pp_soc_gpio.vhd
read_vhdl hardware/rtl/peripherals/pp_soc_memory.vhd
read_vhdl hardware/rtl/peripherals/pp_soc_timer.vhd
read_vhdl hardware/rtl/peripherals/pp_soc_uart.vhd
read_vhdl hardware/rtl/processor/pp_alu.vhd
read_vhdl hardware/rtl/processor/pp_alu_control_unit.vhd
read_vhdl hardware/rtl/processor/pp_alu_mux.vhd
read_vhdl hardware/rtl/processor/pp_comparator.vhd
read_vhdl hardware/rtl/processor/pp_control_unit.vhd
read_vhdl hardware/rtl/processor/pp_core.vhd
read_vhdl hardware/rtl/processor/pp_counter.vhd
read_vhdl hardware/rtl/processor/pp_csr.vhd
read_vhdl hardware/rtl/processor/pp_csr_alu.vhd
read_vhdl hardware/rtl/processor/pp_csr_unit.vhd
read_vhdl hardware/rtl/processor/pp_decode.vhd
read_vhdl hardware/rtl/processor/pp_execute.vhd
read_vhdl hardware/rtl/processor/pp_fetch.vhd
read_vhdl hardware/rtl/processor/pp_fifo.vhd
read_vhdl hardware/rtl/processor/pp_icache.vhd
read_vhdl hardware/rtl/processor/pp_imm_decoder.vhd
read_vhdl hardware/rtl/processor/pp_memory.vhd
read_vhdl hardware/rtl/processor/pp_potato.vhd
read_vhdl hardware/rtl/processor/pp_register_file.vhd
read_vhdl hardware/rtl/processor/pp_writeback.vhd
read_vhdl hardware/rtl/wishbone/pp_soc_intercon.vhd
read_vhdl hardware/rtl/wishbone/pp_wb_adapter.vhd
read_vhdl hardware/rtl/wishbone/pp_wb_arbiter.vhd
read_xdc hardware/constraints/potato_zybo_z7.xdc

# Synthesis and implementation 
synth_design -part xc7z020-clg400-1 -top top_system
opt_design
place_design
route_design

# Performance reports
report_utilization
report_timing

# Create the configuration file
write_vhdl -force synth_system.vhd
write_bitstream -force top_system.bit
