// The Potato Processor Benchmark Applications
// (c) Kristian Klomsten Skordal 2015 <kristian.skordal@wafflemail.net>
// Report bugs and issues on <https://github.com/skordal/potato/issues>

#include <stdint.h>

#include "platform.h"
#include "uart.h"

static struct uart uart0;

void exception_handler(uint32_t cause, void * epc, void * regbase){
    while(uart_tx_fifo_full(&uart0));
    uart_tx(&uart0, 'E'); }

int main(void)
{
    uart_initialize(&uart0, (volatile void *) PLATFORM_UART0_BASE);
    uart_set_divisor(&uart0, uart_baud2divisor(115200, PLATFORM_SYSCLK_FREQ));

    /* Print welcome message */
    uart_tx_string(&uart0, "\n\r** .............. **\n\r");

    uint8_t byte1 = 0xFF;
    uart_tx(&uart0, byte1);

    uint8_t byte2 = 5;
    uart_tx(&uart0, byte2);

    uint32_t byte3 = 0x10203040;
    uart_tx_hex(&uart0, byte3);

    /* Print booting message */
    for(;;){
        uart_tx_string(&uart0, "\n\r** Hello world! **\n\r"); }

    return 0;
}

