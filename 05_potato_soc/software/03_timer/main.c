// The Potato Processor Benchmark Applications
// (c) Kristian Klomsten Skordal 2015 <kristian.skordal@wafflemail.net>
// Report bugs and issues on <https://github.com/skordal/potato/issues>

#include <stdint.h>

#include "platform.h"
#include "uart.h"
#include "gpio.h"
#include "timer.h"

static struct uart  uart0;
static struct gpio  gpio0;
static struct timer timer0;

void delay(uint32_t time){
  uint32_t i = 0;
  while(i < time){
    i++;
    if (i == time){ uart_tx_string(&uart0, "\n"); }}};

void exception_handler(uint32_t cause, void * epc, void * regbase){
    while(uart_tx_fifo_full(&uart0));
    uart_tx(&uart0, 'E'); }


/************************/
/* Main function        */
/************************/
int main(void)
{
  uint32_t gpio_dir = 0x00000F00; // 1 output, 0 input
  uint32_t gpio_val;              // value read from gpio input 
  uint32_t timer_cou = 0x00FFFFFF;// count set in timer
  uint32_t timer_val1;            // value read from timer
  uint32_t timer_val2;            // value read from timer
  uint32_t timer_val3;            // value read from timer
    
  uart_initialize(&uart0, (volatile void *) PLATFORM_UART0_BASE);
  uart_set_divisor(&uart0, uart_baud2divisor(115200, PLATFORM_SYSCLK_FREQ));

  gpio_initialize(&gpio0, (volatile void *) PLATFORM_GPIO_BASE);
  gpio_set_direction(&gpio0, gpio_dir);

  timer_initialize(&timer0,(volatile void *) PLATFORM_TIMER0_BASE);
  timer_set_count(&timer0, timer_cou);
   
  /* Print welcome message */
  uart_tx_string(&uart0, "\n** .............. **\n");
  uart_tx_string(&uart0, "\n** GPIO leds-sws  **\n");
  uart_tx_string(&uart0, "\n** .............. **\n");

  timer_start(&timer0);
  timer_val1 = timer_get_count(&timer0);
  timer_stop(&timer0); timer_start(&timer0);
  timer_val2 = timer_get_count(&timer0);
  timer_stop(&timer0); timer_start(&timer0);
  timer_val3 = timer_get_count(&timer0);
  timer_stop(&timer0);
  uart_tx_string(&uart0, "\n Timer count 1: ");
  uart_tx_hex(&uart0, timer_val1); uart_tx_string(&uart0, "\n"); 
  uart_tx_string(&uart0, "\n Timer count 2: ");
  uart_tx_hex(&uart0, timer_val2); uart_tx_string(&uart0, "\n"); 
  uart_tx_string(&uart0, "\n Timer count 3: ");
  uart_tx_hex(&uart0, timer_val3); uart_tx_string(&uart0, "\n"); 
  
  for (;;){ 
    gpio_val = gpio_get_input(&gpio0);
    uart_tx_hex(&uart0, gpio_val);    uart_tx_string(&uart0, "\n");
    uart_tx_hex(&uart0, gpio_val<<4); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, gpio_val << 4);
    delay(6000000); // 6000000 -> 1 [s]
  }

  return 0;
}

