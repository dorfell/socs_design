// The Potato Processor Benchmark Applications
// (c) Kristian Klomsten Skordal 2015 <kristian.skordal@wafflemail.net>
// Report bugs and issues on <https://github.com/skordal/potato/issues>

#include <stdint.h>

#include "platform.h"
#include "uart.h"
#include "gpio.h"

static struct uart uart0;
static struct gpio gpio0;

void delay(uint32_t time){
  uint32_t i = 0;
  while(i < time){
    i++;
    if (i == time){ uart_tx_string(&uart0, "\r"); }}};

void exception_handler(uint32_t cause, void * epc, void * regbase){
    while(uart_tx_fifo_full(&uart0));
    uart_tx(&uart0, 'E'); }

/************************/
/* Main function        */
/************************/
int main(void)
{
  uint32_t gpio_dir = 0x00000F00; // 1 output, 0 input
    
  uart_initialize(&uart0, (volatile void *) PLATFORM_UART0_BASE);
  uart_set_divisor(&uart0, uart_baud2divisor(115200, PLATFORM_SYSCLK_FREQ));

  gpio_initialize(&gpio0, (volatile void *) PLATFORM_GPIO_BASE);
  gpio_set_direction(&gpio0, gpio_dir);

  /* Print welcome message */
  uart_tx_string(&uart0, "\n** .............. **\n");
  uart_tx_string(&uart0, "\n** GPIO leds      **\n");
  uart_tx_string(&uart0, "\n** .............. **\n");

  for (;;){ 
    uart_tx_hex(&uart0, 0x00000000); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000000);
    delay(6000000); // 6000000 -> 1 [s]

    uart_tx_hex(&uart0, 0x00000100); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000100);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000200); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000200);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000300); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000300);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000400); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000400);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000500); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000500);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000600); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000600);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000700); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000700);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000800); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000800);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000900); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000900);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000A00); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000A00);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000B00); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000B00);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000C00); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000C00);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000D00); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000D00);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000E00); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000E00);  
    delay(6000000); 

    uart_tx_hex(&uart0, 0x00000F00); uart_tx_string(&uart0, "\n");
    gpio_set_output(&gpio0, 0x00000F00);  
    delay(6000000); }

  return 0;
}

