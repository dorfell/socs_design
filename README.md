-------------------------------------------------------------------

     =========================================================
     socs_design - Design of several Systems On-Chip (SoCs)
     =========================================================

                         List of modules
                         ---------------

This project presents the design of different systems on-chip (SoCs) by 
employing several approachs. The project is mainly developed to work 
with reconfigurable platformas such as FPGA. Some of the works aim to 
Zybo-Z7, Nexys4  development boards.


00_arm 
 - Using the ARM  Cortex-A9 hardcore processor with the Vivado IP  block design.
   This is implemented in the Zybo-Z7 development board.

01_arm_axi  
 - Implementing a SoC composed by the ARM  Cortex-A9 procesor and a AXI bus with the 
   Vivado IP  block design in the Zybo-Z7 development board.

02_microblaze_axi_Artix7 
 - Implementing a SoC composed by the MicroBlaze softcore procesor and a AXI bus 
   with the Vivado IP  block design in the Nexys4 development board.

03_picorv32_zyboz7 
 - Implementing a PicoRV32 softcore processor (made by Clifford Wolf) with memory 
   in the Zybo-Z7 development board. (Written in verilog).

04_picosoc_zyboz7 
 - Implementing a SoC with the PicoRV32 softcore processor (made by Clifford Wolf) 
   with the native interface of the processor (i.e. without communication bus)  
   in the Zybo-Z7 development board. (Written in verilog).

05_potato_soc 
 - Implementing a SoC with the Potato processor (made by Cristian Skordal) 
   with a wishbone interface in the Zybo-Z7 development board. (Written in VHDL).
   
06_LiTeX_socs  
 - SoCs from the fpga_101 for LiTeX, designed with Python and implemented in the 
   Zybo-Z7 development board. lab003: SoC composed by an uart brige and peripherals.
   lab004: SoC composed by a softcore processor (e.g. picorv32, vexrisc) a wishbone
   communication bus and peripherals (e.g. leds, switches, etc.).

docs  
 - Documentation for some of the socs designs present in this repository.

hw_doc  
 - Zybo-Z7 development board documentation including physical constraints,
   reference manual, schematics, etc.

videos
 - Videos of some designs implemented in the Zybo-Z7 development board.

README.md
 - You're reading it righ now  8-P. 
