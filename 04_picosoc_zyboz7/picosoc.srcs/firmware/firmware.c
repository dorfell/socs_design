
#include <stdint.h>
#include <stdbool.h>


#define ROM_MEM_TOTAL 0x4000 /* 16 kB */
#define RAM_MEM_TOTAL 0x4000 /* 16 kB */

#define reg_uart_clkdiv (*(volatile uint32_t*)0x02000004)
#define reg_uart_data   (*(volatile uint32_t*)0x02000008)
#define reg_leds        (*(volatile uint32_t*)0x03000000)


/* Functions prototypes                        */
/***********************************************/
void delay(uint32_t time);
void putchar(char c);
void print(const char *p);
void print_hex(uint32_t v, int digits);
void print_dec(uint32_t v);


/* Delay function                              */
/***********************************************/
void delay(uint32_t time){
    uint32_t i = 0;
    while(i < time){ 
        i++;
        if (i == time/2){ 
            putchar('-'); }
    }
};

/* putchar function                            */
/***********************************************/
void putchar(char c){
    reg_uart_data = c; }


/* print function                              */
/***********************************************/
void print(const char *p){
    while (*p)
    putchar(*(p++)); }


/* print_hex function                          */
/***********************************************/
void print_hex(uint32_t v, int digits){
    for (int i = 7; i >= 0; i--) {
        char c = "0123456789abcdef"[(v >> (4*i)) & 15];
        if (c == '0' && i >= digits) continue;
        putchar(c);
	digits = i;
        }
}


/* print_dec function                          */
/***********************************************/
void print_dec(uint32_t v){
    if (v >= 1000) {
        print(">=1000");
	return;
    }

    if      (v >= 900) { putchar('9'); v -= 900; }
    else if (v >= 800) { putchar('8'); v -= 800; }
    else if (v >= 700) { putchar('7'); v -= 700; }
    else if (v >= 600) { putchar('6'); v -= 600; }
    else if (v >= 500) { putchar('5'); v -= 500; }
    else if (v >= 400) { putchar('4'); v -= 400; }
    else if (v >= 300) { putchar('3'); v -= 300; }
    else if (v >= 200) { putchar('2'); v -= 200; }
    else if (v >= 100) { putchar('1'); v -= 100; }

    if      (v >= 90) { putchar('9'); v -= 90; }
    else if (v >= 80) { putchar('8'); v -= 80; }
    else if (v >= 70) { putchar('7'); v -= 70; }
    else if (v >= 60) { putchar('6'); v -= 60; }
    else if (v >= 50) { putchar('5'); v -= 50; }
    else if (v >= 40) { putchar('4'); v -= 40; }
    else if (v >= 30) { putchar('3'); v -= 30; }
    else if (v >= 20) { putchar('2'); v -= 20; }
    else if (v >= 10) { putchar('1'); v -= 10; }

    if      (v >= 9) { putchar('9'); v -= 9; }
    else if (v >= 8) { putchar('8'); v -= 8; }
    else if (v >= 7) { putchar('7'); v -= 7; }
    else if (v >= 6) { putchar('6'); v -= 6; }
    else if (v >= 5) { putchar('5'); v -= 5; }
    else if (v >= 4) { putchar('4'); v -= 4; }
    else if (v >= 3) { putchar('3'); v -= 3; }
    else if (v >= 2) { putchar('2'); v -= 2; }
    else if (v >= 1) { putchar('1'); v -= 1; }
    else putchar('0');
}


/***********************************************/
/* Main function                               */
/***********************************************/
void main(){
    
    // Simple UART Baudrate 
    //reg_uart_clkdiv = 104;// clk = 12  MHz Baudrate: 115200
    reg_uart_clkdiv = 1064; // clk = 125 MHz Baudrate: 115200

    print("Booting..\n");

    
    print("\n");
    print("  ____  _          ____         ____\n");
    print(" |  _ \\(_) ___ ___/ ___|  ___  / ___|\n");
    print(" | |_) | |/ __/ _ \\___ \\ / _ \\| |\n");
    print(" |  __/| | (_| (_) |__) | (_) | |___\n");
    print(" |_|   |_|\\___\\___/____/ \\___/ \\____|\n");
    print("\n");

    print("Total memory: ");
    print_dec(RAM_MEM_TOTAL / 1024);
    print(" kiB \n");
    print(" --- \n");
    

    /* Déclaration des variables */ 
    uint8_t a = 2; uint8_t b = 3; uint8_t c=0; // Pour l'addition        c = a + b

    while(true){

        print("Faire l'addition: \n"); 
        print("--- --- --- \n");
        print("2 + 3 =  \n"); 
        // Tester les operations  arithmétiques 
        c = a + b;  putchar(c); print("\n");

        print("Leds        \n");
        print("--- --- --- \n");
        delay(4000000); reg_leds = 0x00000000;
        delay(4000000); reg_leds = 0x00000091;
        delay(4000000); reg_leds = 0x00000123;
        delay(4000000); reg_leds = 0x00000247;
        delay(4000000); reg_leds = 0x000001BF;
        delay(4000000); reg_leds = 0x00000207;
        delay(4000000); reg_leds = 0x00000363;
        delay(4000000); reg_leds = 0x000003F1;
        print("\n");
    }

}
