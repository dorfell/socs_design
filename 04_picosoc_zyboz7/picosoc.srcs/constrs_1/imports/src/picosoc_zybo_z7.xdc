# SoC  constraints for Zybo Z7
# (c) Dorfell Parra  <dlparrap@unal.edu.co>


# Set operating conditions to improve temperature estimation:
set_operating_conditions -airflow 0
set_operating_conditions -heatsink low 

# Clock signal:
set_property -dict {PACKAGE_PIN K17 IOSTANDARD LVCMOS33} [get_ports {clk}]; 
create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports {clk}]; 

# Reset button: 
set_property -dict {PACKAGE_PIN K14 IOSTANDARD LVCMOS33} [get_ports {resetn}]; 



	output        iomem_valid,
	input         iomem_ready,
	output [ 3:0] iomem_wstrb,
	output [31:0] iomem_addr,
	output [31:0] iomem_wdata,
	input  [31:0] iomem_rdata,

# Pmod Header JC (JC3..JC0)-  JB (JB3..JB0) : leds
set_property PACKAGE_PIN T10     [get_ports {out_byte[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[7]}]
set_property PACKAGE_PIN T11 [get_ports {out_byte[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[6]}]
set_property PACKAGE_PIN W15 [get_ports {out_byte[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[5]}]
set_property PACKAGE_PIN V15 [get_ports {out_byte[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[4]}]
set_property PACKAGE_PIN V7 [get_ports {out_byte[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[3]}]
set_property PACKAGE_PIN U7 [get_ports {out_byte[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[2]}]
set_property PACKAGE_PIN W8 [get_ports {out_byte[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[1]}]
set_property PACKAGE_PIN V8 [get_ports {out_byte[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte[0]}]



# Pmod Header JA (JA0..JA2)

set_property PACKAGE_PIN N15     [get_ports {trap}]
set_property IOSTANDARD LVCMOS33 [get_ports {trap}]
set_property PACKAGE_PIN L14     [get_ports {out_byte_en}]
set_property IOSTANDARD LVCMOS33 [get_ports {out_byte_en}]

# UART0 (PMOD JB):
set_property -dict {PACKAGE_PIN V8  IOSTANDARD LVCMOS33} [get_ports {ser_tx}]; 
set_property -dict {PACKAGE_PIN W8  IOSTANDARD LVCMOS33} [get_ports {ser_rx}]; 
