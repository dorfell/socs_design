# SoC  constraints for Zybo Z7
# (c) Dorfell Parra  <dlparrap@unal.edu.co>


# Set operating conditions to improve temperature estimation:
set_operating_conditions -airflow 0
set_operating_conditions -heatsink low 

# Clock signal:
set_property -dict { PACKAGE_PIN K17 IOSTANDARD LVCMOS33 } [get_ports { clk }]; 
create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports {clk}]; 

# Reset button (JA PMOD): 
set_property -dict { PACKAGE_PIN K16 IOSTANDARD LVCMOS33 } [ get_ports { resetn      }]; 
set_property -dict { PACKAGE_PIN N15 IOSTANDARD LVCMOS33 } [ get_ports { iomem_valid }];

# UART0 (JB PMOD):
set_property -dict { PACKAGE_PIN V8  IOSTANDARD LVCMOS33 } [ get_ports {ser_tx} ]; 
set_property -dict { PACKAGE_PIN W8  IOSTANDARD LVCMOS33 } [ get_ports {ser_rx} ]; 

##LEDs
set_property -dict { PACKAGE_PIN M14   IOSTANDARD LVCMOS33 } [ get_ports { leds[0] }]; #IO_L23P_T3_35 Sch=led[0]
set_property -dict { PACKAGE_PIN M15   IOSTANDARD LVCMOS33 } [ get_ports { leds[1] }]; #IO_L23N_T3_35 Sch=led[1]
set_property -dict { PACKAGE_PIN G14   IOSTANDARD LVCMOS33 } [ get_ports { leds[2] }]; #IO_0_35 Sch=led[2]
set_property -dict { PACKAGE_PIN D18   IOSTANDARD LVCMOS33 } [ get_ports { leds[3] }]; #IO_L3N_T0_DQS_AD1N_35 Sch=led[3]

##RGB LED 5 (Zybo Z7-20 only)
set_property -dict { PACKAGE_PIN Y11   IOSTANDARD LVCMOS33 } [ get_ports { leds[4] }]; #IO_L18N_T2_13 Sch=led5_r
set_property -dict { PACKAGE_PIN T5    IOSTANDARD LVCMOS33 } [ get_ports { leds[5] }]; #IO_L19P_T3_13 Sch=led5_g
set_property -dict { PACKAGE_PIN Y12   IOSTANDARD LVCMOS33 } [ get_ports { leds[6] }]; #IO_L20P_T3_13 Sch=led5_b

##RGB LED 6
set_property -dict { PACKAGE_PIN V16   IOSTANDARD LVCMOS33 } [ get_ports { leds[7] }]; #IO_L18P_T2_34 Sch=led6_r
set_property -dict { PACKAGE_PIN F17   IOSTANDARD LVCMOS33 } [ get_ports { leds[8] }]; #IO_L6N_T0_VREF_35 Sch=led6_g
set_property -dict { PACKAGE_PIN M17   IOSTANDARD LVCMOS33 } [ get_ports { leds[9] }]; #IO_L8P_T1_AD10P_35 Sch=led6_b
