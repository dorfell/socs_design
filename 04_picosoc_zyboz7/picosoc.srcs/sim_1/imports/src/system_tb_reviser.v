`timescale 1 ns / 1 ps

module system_tb;
  
    wire         iomem_ready;
    wire         iomem_valid;
    wire  [ 3:0] iomem_wstrb;
    wire  [31:0] iomem_addr;
    wire  [31:0] iomem_wdata;
    wire  [31:0] iomem_rdata;
	
    wire              ser_rx;
    wire              ser_tx;

  
    // Clock stimulus
    reg clk = 1;
    always #5 clk = ~clk;
  
    // Reset
    reg resetn = 0;
    initial begin
      repeat (10) @(posedge clk);
  	  resetn <= 1;
    end
 
    //assign iomem_ready = 1;

    system uut (
        .clk          (clk        ),
	    .resetn       (resetn     ),
	
    	//.iomem_ready  (iomem_ready),
    	.iomem_ready  (1'b1),
	    .iomem_valid  (iomem_valid),
	    .iomem_wstrb  (iomem_wstrb),
	    .iomem_addr   (iomem_addr ),
	    .iomem_wdata  (iomem_wdata),
	    .iomem_rdata  (iomem_rdata),
	
	    .ser_rx       (ser_rx),
	    .ser_tx       (ser_tx)
    );

endmodule