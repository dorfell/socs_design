`timescale 1 ns / 1 ps

module picosoc_tb;
	
	// Clock stimulus
	reg clk = 1;
	always #4 clk = ~clk; // 125 MHz
	//always #5 clk = ~clk; // 100 MHz
	//always #42 clk = ~clk; // 12 MHz

    // Reset declaration
	reg resetn = 0;
	initial begin
		repeat (2) @(posedge clk);
		resetn <= 1;
	end

    // Input and outputs UUT
	wire        iomem_valid;
	wire        iomem_ready;
	wire [ 3:0] iomem_wstrb;
	wire [31:0] iomem_addr;
	wire [31:0] iomem_wdata;
	wire [31:0] iomem_rdata;
	
	wire             ser_rx;
	wire             ser_tx;	

    // Instance
	picosoc uut (
		.clk          (clk),
		.resetn       (resetn),		
	    .iomem_valid  (iomem_valid),
	    .iomem_ready  (1'b0),
	    .iomem_wstrb  (iomem_wstrb),
	    .iomem_addr   (iomem_addr),
	    .iomem_wdata  (iomem_wdata),
	    .iomem_rdata  (32'h0000_0000),
	
	    .ser_rx       (1'b0),
	    .ser_tx       (ser_tx)	
	);


endmodule
