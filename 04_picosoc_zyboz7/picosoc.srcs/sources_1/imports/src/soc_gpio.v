`timescale 1 ns / 1 ps

module soc_gpio(
	input                 clk,
	input              resetn,
		
	input             iomem_valid,
	output reg        iomem_ready,
	input      [ 3:0] iomem_wstrb,
	input      [31:0] iomem_addr,
	input      [31:0] iomem_wdata,
	output reg [31:0] iomem_rdata,
		 	
	output [9:0] leds
		
);
        
	reg [31:0] gpio;
	assign leds = gpio[9:0];


	always @(posedge clk) begin
		if (!resetn) begin
			gpio <= 32'b0;
		end else begin
			iomem_ready <= 0;
			if (iomem_valid && !iomem_ready && iomem_addr[31:24] == 8'h 03) begin
				iomem_ready <= 1;
				iomem_rdata <= gpio;
				if (iomem_wstrb[0]) gpio[ 7: 0] <= iomem_wdata[ 7: 0];
				if (iomem_wstrb[1]) gpio[15: 8] <= iomem_wdata[15: 8];
				if (iomem_wstrb[2]) gpio[23:16] <= iomem_wdata[23:16];
				if (iomem_wstrb[3]) gpio[31:24] <= iomem_wdata[31:24];
			end
		end
	end	
	
endmodule

