`timescale 1 ns / 1 ps

module system (
	input        clk,
	input        resetn,

    output       iomem_valid,
    
	output       ser_tx,
	input        ser_rx,
	
	output [9:0] leds
);

    parameter integer ROM_MEM_WORDS = 4096;           // 4096 x 4B = 16 kB 
	parameter integer RAM_MEM_WORDS = 4096;           // 4096 x 4B = 16 kB
	
	//wire        iomem_valid;
	wire        iomem_ready;
	wire [3:0]  iomem_wstrb;
	wire [31:0] iomem_addr;
	wire [31:0] iomem_wdata;
	wire [31:0] iomem_rdata;

    assign iomem_valid = iomem_valid;
    
    
    // PicoSoC instance
	picosoc #(
		.BARREL_SHIFTER(0),
		.ENABLE_MULDIV(0),
		.ROM_MEM_WORDS(ROM_MEM_WORDS),
		.RAM_MEM_WORDS(RAM_MEM_WORDS)
	) soc (
		.clk          (clk          ),
		.resetn       (resetn       ),

		.ser_tx       (ser_tx       ),
		.ser_rx       (ser_rx       ),

		.iomem_valid  (iomem_valid  ),
		.iomem_ready  (iomem_ready  ),
		.iomem_wstrb  (iomem_wstrb  ),
		.iomem_addr   (iomem_addr   ),
		.iomem_wdata  (iomem_wdata  ),
		.iomem_rdata  (iomem_rdata  )
	);
	
    // GPIO instance
	soc_gpio gpio (
		.clk          (clk          ),
		.resetn       (resetn       ),

		.iomem_valid  (iomem_valid  ),
		.iomem_ready  (iomem_ready  ),
		.iomem_wstrb  (iomem_wstrb  ),
		.iomem_addr   (iomem_addr   ),
		.iomem_wdata  (iomem_wdata  ),
		.iomem_rdata  (iomem_rdata  ),
		
		.leds         (leds         )
		
	);
	
endmodule
