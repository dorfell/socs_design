`timescale 1 ns / 1 ps


module rom_memory #(
	parameter integer WORDS = 4096 // 4096 x 4B = 16 kB 
	) (
	input clk,
	input [23:0] addr,
	input [31:0] wdata,
	
	output reg [31:0] rdata
);

	reg [31:0] mem [0:WORDS-1];
    initial $readmemh("/home/dorfell/Documents/Vitis_2019.2/socs_design/04_picosoc_zyboz7/picosoc.srcs/firmware/firmware.hex", mem);

	always @(posedge clk) begin
		rdata <= mem[addr>>2];
	end
endmodule


module ram_memory #(
	parameter integer WORDS = 256 // 256 x 4B = 1 kB 
	) (
	input clk,
	input [ 3:0]   wen,
	input [23:0]  addr,
	input [31:0] wdata,
	output reg [31:0] rdata
);
	reg [31:0] mem [0:WORDS-1];
    
	always @(posedge clk) begin
		rdata <= mem[addr>>2];
		if (wen[0]) mem[addr>>2][ 7: 0] <= wdata[ 7: 0];
		if (wen[1]) mem[addr>>2][15: 8] <= wdata[15: 8];
		if (wen[2]) mem[addr>>2][23:16] <= wdata[23:16];
		if (wen[3]) mem[addr>>2][31:24] <= wdata[31:24];
	end
endmodule