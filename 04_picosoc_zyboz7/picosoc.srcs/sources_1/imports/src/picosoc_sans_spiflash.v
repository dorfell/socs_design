`timescale 1 ns / 1 ps

module picosoc (
	input                 clk,
	input              resetn,
		
	output        iomem_valid,
	input         iomem_ready,
	output [ 3:0] iomem_wstrb,
	output [31:0] iomem_addr,
	output [31:0] iomem_wdata,
	input  [31:0] iomem_rdata,
	
	input              ser_rx,
	output             ser_tx	
);

    parameter [0:0] BARREL_SHIFTER    = 1;
	parameter [0:0] ENABLE_MULDIV     = 1;
	parameter [0:0] ENABLE_COMPRESSED = 1;
	parameter [0:0] ENABLE_COUNTERS   = 1;
	parameter [0:0] ENABLE_IRQ_QREGS  = 0;

	parameter integer ROM_MEM_WORDS = 4096;           // 4096 x 4B = 16 kB 
	parameter integer RAM_MEM_WORDS = 4096;           // 4096 x 4B = 16 kB
	parameter [31:0] STACKADDR = (4*RAM_MEM_WORDS);   // end of memory  4B x 4096 = 16 kB
	parameter [31:0] PROGADDR_RESET = 32'h0000_0000;  // Reset to the 16 kB ROM memory
	parameter [31:0] PROGADDR_IRQ   = 32'h0000_0000;  // The start address of the interrupt handler.

	wire mem_valid;
	wire mem_instr;
	wire mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [ 3:0] mem_wstrb;
	wire [31:0] mem_rdata;
	
	reg  rom_ready;
	wire [31:0] rom_rdata;
	
    reg  ram_ready;
    wire [31:0] ram_rdata;
    
	assign iomem_valid = mem_valid && (mem_addr[31:24] > 8'h01);
	assign iomem_wstrb = mem_wstrb;
	assign iomem_addr  = mem_addr;
	assign iomem_wdata = mem_wdata;

	wire        simpleuart_reg_div_sel = mem_valid && (mem_addr == 32'h0200_0004);
	wire [31:0] simpleuart_reg_div_do;

	wire        simpleuart_reg_dat_sel = mem_valid && (mem_addr == 32'h0200_0008);
	wire [31:0] simpleuart_reg_dat_do;
	wire        simpleuart_reg_dat_wait;

	assign mem_ready = (iomem_valid && iomem_ready) || rom_ready || ram_ready || 
	                   simpleuart_reg_div_sel || 
	                   (simpleuart_reg_dat_sel && !simpleuart_reg_dat_wait);

	assign mem_rdata = (iomem_valid && iomem_ready) ? iomem_rdata : rom_ready ? rom_rdata : ram_ready ? ram_rdata : 
	                   simpleuart_reg_div_sel ? simpleuart_reg_div_do : simpleuart_reg_dat_sel ?
	                   simpleuart_reg_dat_do : 32'h0000_0000;
	
	
	picorv32 #(
		.STACKADDR(STACKADDR),
		.PROGADDR_RESET(PROGADDR_RESET),
		.PROGADDR_IRQ(PROGADDR_IRQ),
		.BARREL_SHIFTER(BARREL_SHIFTER),
		.COMPRESSED_ISA(ENABLE_COMPRESSED),
		.ENABLE_COUNTERS(ENABLE_COUNTERS),
		.ENABLE_MUL(ENABLE_MULDIV),
		.ENABLE_DIV(ENABLE_MULDIV),
		.ENABLE_IRQ(1),
		.ENABLE_IRQ_QREGS(ENABLE_IRQ_QREGS)
	) cpu (
		.clk         (clk        ),
		.resetn      (resetn     ),
		.mem_valid   (mem_valid  ),
		.mem_instr   (mem_instr  ),
		.mem_ready   (mem_ready  ),
		.mem_addr    (mem_addr   ),
		.mem_wdata   (mem_wdata  ),
		.mem_wstrb   (mem_wstrb  ),
		.mem_rdata   (mem_rdata  ),
		.irq         (32'h0000_0000)
	);
	
	
	// Simple UART peripheral	
	simpleuart simpleuart (
		.clk         (clk         ),
		.resetn      (resetn      ),

		.ser_tx      (ser_tx      ),
		.ser_rx      (ser_rx      ),

		.reg_div_we  (simpleuart_reg_div_sel ? mem_wstrb : 4'b0000),
		.reg_div_di  (mem_wdata),
		.reg_div_do  (simpleuart_reg_div_do),

		.reg_dat_we  (simpleuart_reg_dat_sel ? mem_wstrb[0] : 1'b0),
		.reg_dat_re  (simpleuart_reg_dat_sel && !mem_wstrb),
		.reg_dat_di  (mem_wdata),
		.reg_dat_do  (simpleuart_reg_dat_do),
		.reg_dat_wait(simpleuart_reg_dat_wait)
	);


	// ROM memory 
	always @(posedge clk)
	    rom_ready <= mem_valid && !mem_ready && mem_addr >= 32'h0000_0000 && mem_addr < 4*ROM_MEM_WORDS; //1 cycle rdata
	rom_memory #(
		.WORDS(ROM_MEM_WORDS)
	) rom_mem (
		.clk(clk),
		.addr(mem_addr[23:0]),
		.wdata(mem_wdata),
		.rdata(rom_rdata)
	);
	
	
	// RAM memory 
	always @(posedge clk)
	    ram_ready <= mem_valid && !mem_ready && (mem_addr >= 32'h0100_0000) && mem_addr < (32'h0100_0000 + 4*RAM_MEM_WORDS);
	ram_memory #(
		.WORDS(RAM_MEM_WORDS)
	) ram_mem (
		.clk(clk),
		.wen((mem_valid && !mem_ready && (mem_addr >= 32'h0100_0000) && mem_addr < (32'h0100_0000 + 4*RAM_MEM_WORDS) ) ? mem_wstrb : 4'b0),
		.addr(mem_addr[23:0]),
		.wdata(mem_wdata),
		.rdata(ram_rdata)
	);
			
endmodule

