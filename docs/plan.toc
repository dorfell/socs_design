\nobreakspace {}\hfill \textbf {Pag.}\par 
\contentsline {chapter}{Contents}{ii}{section*.1}% 
\contentsline {chapter}{List of Figures}{iv}{section*.2}% 
\contentsline {chapter}{List of Tables}{vi}{section*.3}% 
\contentsline {section}{INTRODUCTION}{1}{section*.4}% 
\contentsline {chapter}{\chapternumberline {1}Tools Installation}{1}{chapter.1}% 
\contentsline {section}{\numberline {1}Installing Vitis}{1}{section.1.1}% 
\contentsline {subsection}{\numberline {1.1}Supported Operating System}{1}{subsection.1.1.1}% 
\contentsline {subsection}{\numberline {1.2}Download and Installation}{1}{subsection.1.1.2}% 
\contentsline {subsection}{\numberline {1.3}Installing Digilent drivers}{7}{subsection.1.1.3}% 
\contentsline {subsection}{\numberline {1.4}Launching Vivado}{7}{subsection.1.1.4}% 
\contentsline {subsection}{\numberline {1.5}Launching Vitis}{7}{subsection.1.1.5}% 
\contentsline {section}{\numberline {2}Installing RISCV toolchain}{7}{section.1.2}% 
\contentsline {section}{\numberline {3}Installing Serial Communication tools}{7}{section.1.3}% 
\contentsline {section}{\numberline {4}Installing Configuring tools}{7}{section.1.4}% 
\contentsline {chapter}{\chapternumberline {2}Lab 01: The ARM processor}{8}{chapter.2}% 
\contentsline {section}{\numberline {1}Introduction}{8}{section.2.1}% 
\contentsline {subsection}{\numberline {1.1}Devices and Programs}{8}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {1.2}Summarized Steps}{9}{subsection.2.1.2}% 
\contentsline {section}{\numberline {2}Building a Zynq-7000 SoC Processor Design}{9}{section.2.2}% 
\contentsline {section}{\numberline {3}Vitis Application Process}{18}{section.2.3}% 
\contentsline {chapter}{\chapternumberline {3}Lab 02: ARM processor with AXI Lite bus}{25}{chapter.3}% 
\contentsline {section}{\numberline {1}Introduction}{25}{section.3.1}% 
\contentsline {subsection}{\numberline {1.1}Devices and Programs}{26}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {1.2}Summarized Steps}{26}{subsection.3.1.2}% 
\contentsline {section}{\numberline {2}Vivado Block Design Process}{27}{section.3.2}% 
\contentsline {section}{\numberline {3}Vitis Application Process}{35}{section.3.3}% 
\contentsline {chapter}{\chapternumberline {4}Lab 02: MicroBlaze with AXI Lite bus}{39}{chapter.4}% 
\contentsline {section}{\numberline {1}Introduction}{39}{section.4.1}% 
\contentsline {subsection}{\numberline {1.1}Devices and Programs}{40}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {1.2}Summarized Steps}{40}{subsection.4.1.2}% 
\contentsline {section}{\numberline {2}Vivado Block Design Process}{41}{section.4.2}% 
\contentsline {chapter}{Bibliography}{54}{chapter*.79}% 
