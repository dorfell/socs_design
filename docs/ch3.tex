%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lab 02: ARM processor with AXI Lite bus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Lab 02: ARM processor with AXI Lite bus}
\label{ch:03_arm_axi}

% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
This lab presents the basic design flow to implement an ARM SoC with an AXI bus in a Zybo Z7 board. This implementation is made with the Vivado block design tool which generate the hardware design, and the Vitis software platform that allows to create an application to be executed for the hardware design. \\

In this implementation the ARM processor will be known as the processing system and the others components including the  AXI Bus interconnect and the AXI peripherals will be implemented in the programmable logic.\\


\textbf{Processing System: Cortex-A9 Hardcore Processor}
The processing system include several peripherals that can be used with the ARM processor through multiplexed input/output (MIO) pins. In this lab the following peripherals will be used:


\definecolor{tcA}{rgb}{0.827451,0.843137,0.811765}
\begin{center}
\begin{tabular}{ll}\hline
  % use packages: color,colortbl
  \rowcolor{tcA}
  Peripheral & MIO Pin\\\hline
  UART       & 48-49  \\
  Buttons    & 50-51  \\
  LED        & 07     \\\hline
\end{tabular}
\end{center}



\textbf{Programmable Logic: Zynq XCZ7020}

\definecolor{tcA}{rgb}{0.827451,0.843137,0.811765}
\begin{center}
\begin{tabular}{ll}\hline
  % use packages: color,colortbl
  \rowcolor{tcA}
  Peripheral   & FPGA Pin\\\hline
  LEDs         & 00  \\
  RGBs         & 00  \\
  Switches     & 00  \\
  Buttons      & 00  \\
  Uart Lite Tx & 00  \\
  Uart Lite Rx & 00  \\\hline
\end{tabular}
\end{center}



\subsection{Devices and Programs}
\begin{itemize}
    \item Zybo Z7
    \item USB Serial module
    \item Vivado Design Suite
    \item Vitis software Platform
    \item Graphical Serial Terminal (Cutecom for Linux)
\end{itemize}


\subsection{Summarized Steps}
\textbf{Vivado Design Suite}
\begin{enumerate}
    \item Create block design
    \item Add components on the design
    \item Run Block Automation and Run Connection Automation
    \item Create Wrapper
    \item Create XDC
    \item Associate the wrapper pins names to the ports names in XDC
    \item Generate BitStream
    \item Export Hardware
\end{enumerate}


\textbf{Vitis software Platform}
\begin{enumerate}
    \item Import Hardware (.XSA)
    \item Select OS
    \item Write Code
    \item Build Project
    \item Program FPGA
    \item Launch on Hardware
\end{enumerate}


% Vivado Block Design Process
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vivado Block Design Process} 


\begin{enumerate}

\item \textbf{ Add Elements to Block Design}
    After creating a new project and block design, add to the diagram the next blocks:
    \begin{itemize}
        \item Zynq Processing system 
        \item AXI Interconnect
        \item AXI GPIO 0
        \item AXI GPIO 1
        \item AXI UART Lite
    \end{itemize}
    
    To add blocks click on the Add IP button (the one with the plus icon).

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{img/t2/02_00_plus.eps}
      \caption{Vivado Design Suite. Add new elements to the Diagram.}
      \label{fig:02_plus}
    \end{figure}        

    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_01_ip_blocks.eps}
      \caption{Vivado Design Suite. Elements Blocks.}
      \label{fig:01_blocks}
    \end{figure}
    

\item \textbf{Configured Elements}
    
    \textbf{The processing system} should be configured with the steps described in the Lab. 01: The ARM Processor. 
    
    It is mandatory to set the master option, as seen in the figure \ref{fig:02_master}, and set the PL fabric clocks to supply a synchronized signal clock to FPGA, as seen in the figure \ref{fig:02_pl}.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_02_master.eps}
      \caption{Vivado Design Suite. Zynq processing System block - Select master option.}
      \label{fig:02_master}
    \end{figure}

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.8\textwidth]{img/t2/02_03_pl.eps}
      \caption{Vivado Design Suite. Zynq processing System block - select FCLK-CLK0.}
      \label{fig:02_pl}
    \end{figure}
    
    
    \textbf{AXI interconnect block.} You should change the number of master interfaces, set one, referring to the Zynq processing system.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{img/t2/02_04_axiblock.eps}
      \caption{Vivado Design Suite. AXI interconnect block - select one master interfaces.}
      \label{fig:02_axiblock}
    \end{figure}

    \textbf{GPIO AXI 0.} You should write in the GPIO width field the number of Led that you will use and set all outputs. After that, you should configure the GPIO 2 channel section, set enable dual channel, write in the GPIO field the number of switches and set all inputs.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{img/t2/02_05_gpioaxi0.eps}
      \caption{Vivado Design Suite. AXI GPIO 0 block - select AXI GPIO 0 width.}
      \label{fig:02_gpioax0}
    \end{figure}


    \textbf{GPIO AXI 1.} You should write in the GPIO width field the number of pins that use the RGB Leds that you will use and set all outputs. After that, you should configure the GPIO 2 channel section, set enable dual channel, write in the GPIO width field the number of buttons that you will use and set all inputs.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{img/t2/02_06_gpioaxi1.eps}
      \caption{Vivado Design Suite. GPIO AXI block - select AXI GPIO width.}
      \label{fig:02_gpioax1}
    \end{figure}
    
    \textbf{AXI Uart Lite.} You should set the baud rate and parity configuration that you required. 
    
    \begin{figure}[H]
      \centering
      07\includegraphics[width=0.7\textwidth]{img/t2/02_07_uartlite.eps}
      \caption{Vivado Design Suite. Uart Lite Configuration.}
      \label{fig:02_uartlite}
    \end{figure}

 
    \item \textbf{Generate Connections Blocks}
    Click on ``Run Block Automation'' to generate the reset block component, and next click on ``Run Connection Automation'' to generate the connection between the block in the design.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_08_run_automation.eps}
      \caption{Vivado Design Suite. Generate Connections Blocks in the Design Block.}
      \label{fig:02_r_automation}
    \end{figure}


    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_09_automation.eps}
      \caption{Vivado Design Suite. Block Design with Hardcore processor, and GPIO and Uart peripheral interconnected with AXI Bus - Zybo Z7.}
      \label{fig:02_automation}
    \end{figure}

    \item \textbf{Create HDL Wrapper}
    You should go to the source window, right click on block design file ($blockdesign\_Name.bd$) and click on ``Create HDL Wrapper''. The file generate is in verilog language for default and you can open with double click on  ($blockdesign\_Name\_Wrapper.v$).

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.4\textwidth]{img/t2/02_10_wrapper.eps}
      \caption{Vivado Design Suite. Generate Wrapper.}
      \label{fig:02_wrapper}
    \end{figure}

    
    \item \textbf{Generate Xilinx Design Constraints (XDC)}
    The XDC file replace the old UCF constraint file and is generate click on plus icon in the source window. You should set ``Add or Create Constraint'' in  the ``Add Source'' window. 
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{img/t2/02_11_add_source.eps}
      \caption{Vivado Design Suite. Add Source Window - Add Constraint File.}
      \label{fig:02_source}
    \end{figure}

    Next, you should paste the definition of the pins that you will use or upload the ``$ZYBO\_Master.xdc$'' which you can looking for on internet. After that, you need change the port names that it should correspond to the names in the wrapper file. as seen the figure \ref{xdc_wrapper} and \ref{xdc_file}.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_12_xdc_wrapper.eps}
      \caption{Vivado Design Suite. Wrapper file}
      \label{fig:02_xdc_wrapper}
    \end{figure}

    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_13_xdc_file.eps}
      \caption{Vivado Design Suite. XDC File}
      \label{fig:02_xdc_file}
    \end{figure}

    
    \item \textbf{Synthesis, Implementation and BitStream}
    Click on ``Generate Bitstream'', and click yes in the ``No implementation Results Available'' window and click Ok in the ``Launch Runs'' window.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{img/t2/02_14_implementation.eps}
      \caption{Vivado Design Suite. No implementation Results Available Window}
      \label{fig:02_implementation}
    \end{figure}

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.5\textwidth]{img/t2/02_15_launch_window.eps}
      \caption{Vivado Design Suite. Launch Runs Window}
      \label{fig:02_launch_window}
    \end{figure}
    
    The result in this step you can see the logic resources which are used by the implementation.

    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_16_resources.eps}
      \caption{Vivado Design Suite. Device Implementation.}
      \label{fig:02_resources}
    \end{figure}

    
    \item \textbf{ Export Hardware}
    The last Step is export the hardware click on File and click Export. In the window ``Export Hardware'' you should set include bitstream and click Ok. 
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.4\textwidth]{img/t2/02_17_export_hd.eps}
      \caption{Vivado Design Suite. Export Hardware Window}
      \label{fig:02_export_hd}
    \end{figure}

\end{enumerate}




% Vitis Application Process
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vitis Application Process} 

\begin{enumerate}

    \item \textbf{Import Hardware}
    When you create a new application project you can choose the platform that you will use, in this section you should click on ``Create a new platform from hardware (XSA)'' and click on plus icon to select the XSA file that was generated in the last step with Vivado Design Suite.

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.86\textwidth]{img/t2/02_18_import_hd.eps}
      \caption{Vitis Software Platform. Import Hardware - New application project Window}
      \label{fig:02_import_hd}
    \end{figure}
    
    \item \textbf{Select OS}
    Set the Stand Alone OS and CPU ``$ps7\_cprtexa9\_0$''.

    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{img/t2/02_19_os.eps}
      \caption{Vitis Software Platform. Select OS - New application project Window}
      \label{fig:02_os}
    \end{figure}    

    \item \textbf{Write your Code}
    The libraries that you need are in the table \ref{tab:02_lib}.
    
    \begin{table}[H]
    \centering
    \begin{tabular}{|l|c|}
      \hline
      \multicolumn{1}{|c|}{\textbf{Libraries}} & \textbf{Peripheral}            \\\hline
      xgpiops.h                                & GPIO Hardcore                  \\\hline
      $xil\_printf.h$                          & UART Hardcore Module           \\\hline
      xparameters.h                            & AXI Addres                     \\\hline
      xgpio.h                                  & GPIO Programmable Logic        \\\hline
      xuartlite.h                              & UART Module Programmable Logic \\\hline
    \end{tabular}
    \caption{Libraries - Vitis Application Project}
    \label{tab:02_lib}
    \end{table}

    \item \textbf{Build Project}
    Click on project name on the explorer window, after that, go to the tool bar and click on project and click ``Build project''.
 
    
    \item \textbf{Launch Hardware}
    Right click on project name on the explorer window, select ``Run as'' and click  on ``Launch on Hardware''.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{img/t2/02_20_launch_fpga.eps}
      \caption{Vitis Software Platform. Launch on Hardware.}
      \label{fig:02_launch_fpga}
    \end{figure}    
    
\end{enumerate}


