%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Proposed Research 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Proposed Research}
\label{ch:03_prsh}
 
    
\section{Problem Statement} 
Neural networks are a powerful tool for the solution of many problems in different 
areas like Machine Vision and Image Processing among others \cite{demant2013}. 
Nowadays,  NN are implemented by means of software libraries such as
Caffe \cite{cf2017}, TensorFlow \cite{tf2017}, Theano \cite{th2017}, and its performance
is measured with the network accuracy, the execution time and the computational
complexity \cite{venieris2016}.\\
In recent years, several works that aim to implement discrete neural networks in 
hardware have appeared. Some of those works use GPU-based platforms like \cite{krizhevsky2014}, 
\cite{chetlur2014}, however, according to the literature GPUs can be a good option only 
for CNN training in non-autonomous systems because of their high throughput and high
energy consumption, but an inadequate one for the 
feedforward part due to their small local memory and limited bandwidth communication
\cite{dundar2016}. Fortunately, the custom hardware is an advantageous option for HNN 
implementations due to the low power consumption and the high performance that can be
achieved. These works that can be found in the literature can be classified in two main
streams; ones that propose accelerators (e.g. \cite{luo2016}, \cite{chen2014}, \cite{zhou2015}) and others that propose 
frameworks (e.g. \cite{venieris2016}, \cite{li2016}, \cite{dundar2016}). The accelerator works 
are oriented to digital circuits composed of adders, multipliers and ALUs that 
evaluate the neural network by using the weights and biases previously computed in
the training process, and  these are usually implemented in configurable FPGA-based
platforms. On the other side, the frameworks available in the literature have as a 
goal to make easier the CNN implementation in hardware.  That is, after defining the 
NN model, the layers and the input dataset, the frameworks intent to  create the 
source files for a possible accelerator. Unfortunately, the training process is still 
made by means of software libraries that execute it in CPU/GPU based systems, which 
are unfeasible or expensive for several industry applications that require autonomous 
embedded systems. Furthermore, the actual frameworks lack of simplicity, aim to 
biggest platforms with high energy consumption rates  and moreover, there isn't a 
consensus in how to design the frameworks or the presented information lacks of 
detail for using and adapting them.
One way to work around this problem is to propose a new framework, that integrates 
the current  NN software libraries and the training process in a hardware-software 
architecture, which target to low-cost hardware platforms and  give advice about the 
trade-offs needed to improve the systems performance, while keeping the simplicity 
of the framework.


  

\section{Motivation and Justification}
Nowadays, the country is buying a lot of technological products needed for its 
development. Unfortunately, the inner technology is not being transferred neither  
assimilated. That is, it is unknown how the core technology work, how it can be 
seized or even how it can be improved, which are weaknesses that stop the country 
development. Part of this new technology are the implementation of neural networks
in hardware (NNHs) which are a very attractive technology that can be used in industry,
image processing, and machine vision among others fields \cite{widrow1994}, 
\cite{misra2010}, \cite{demant2013}, but which is limited to the feed forward execution
(i.e. training made in software) or point to expensive and high energy consumption systems 
(e.g. CPU/GPU). Fortunately, in the last few years the number of available NN libraries 
and low-cost reconfigurable embedded systems, such as FPGA-based 
platforms, needed for their implementation has increased, opening the exploration space
of alternative training platforms.\\
This project intends to contribute to close the existing gap in the implementation of 
the training process of CNN in hardware and help to the technology transference process,
targeting to autonomous embedded systems which are irreplaceable in several industry 
applications. Furthermore, it is expected to have a deeper understanding of HNNs and
the parameters that mostly affect its performance. For these reasons, the main objective
in this project is the proposal of a cost-effective HNNs implementation framework for 
FER systems, that will be based on the different neural networks accelerators and the 
relevant works that can be found in the literature. In the end, the framework will be  
a useful tool for the solution of different problems by means of HNNs.

This work is organized as follows: first, an exhaustive review of the literature is 
made in chapter \ref{ch:02_soa}.
After that, the study, comparison between several CNN models, and the selection
of the most feasible types of layers will be made. Then, a basic CNN implementation
for FER systems will be created, by means of the software libraries available for 
training the NN.
Next, the HNN implementations will be made and evaluated to improve the accelerators
and the processing flow. Following, the CNN training process will be made by using 
a hardware-software architecture and finally, all of these stages will be integrated
in the proposed framework  and it will be validated by using the metrics above 
mentioned (see section \ref{sec:hnn_fwk}).



\section{Objectives}

\subsection{General Objective}
  \begin{itemize}
    \item [$\bullet$] Propose a framework that allows to train and execute a CNN in an
                      embedded system (hardware-software), being able to use custom or the
                      most popular software libraries. The framework will 
                      create the files needed to describe the architecture and will 
                      provide the tools for simulating  and implementing it.
                      Moreover, the framework will give advice about the 
                      hardware-software trade-offs needed to improve the performance.
  \end{itemize}
    

\subsection{Specific Objectives }
  \begin{itemize}

    \item [$\bullet$] Design and implement a basic FER application based on CNNs
                      by means of software libraries.
                      
    \item [$\bullet$] Design and evaluate accelerators for implementing the CNN feed 
                      forward execution in hardware.

    \item [$\bullet$] Design and validate a hardware-software  architecture that implement
                      the training algorithm and fits in an autonomous
                      embedded system.
                      
    \item [$\bullet$] Design and implement a framework that integrates the previously
                      defined objectives and employs a design space exploration to 
                      propose the most cost-effective hardware.

    \item [$\bullet$] Evaluate the framework performance by means of metrics used 
                      in the literature such as the neural network accuracy and the 
                      execution time.
  \end{itemize}
  
  
\subsection{Range of Research}
  \begin{itemize}
    \item [$\bullet$] The state of the art review will focus on the most relevant HNN 
                      frameworks that can be found in the literature, identifying the 
                      processing flows proposed, the design exploration space search 
                      applied and the tools provided. 
  
    \item [$\bullet$] The neural networks types (e.g. the Multi-Layer Perceptron (MLP), 
                      Convolutional Neural Networks (CNN)) 
                      will be studied to compare and analyze their characteristics, 
                      advantages and disadvantages. Hence, the most feasible ones will 
                      be selected by taking into account the amount of resources need,
                      the execution time, and their complexity among others.
                          
    \item [$\bullet$] A basic FER application based on CNNs will be made by using some 
                      of the software libraries available. Hence, the current 
                      implementation limitations will be identified and will help to 
                      define the framework co-design.
                                                
    \item [$\bullet$] The most important accelerators present in the state of the art
                      will be studied to identify the neural network mapping  process, 
                      the architectures used, their performance, and the amount of 
                      resources used. 
                      
    \item [$\bullet$] The fundamental architecture of the accelerators will be defined.
                      This will be composed of two main components: software (e.g. 
                      CPU, DSP) and hardware (e.g. FPGAs) which will be design to map 
                      the network layers into hardware. Also, in this stage the 
                      performance computation and simulation of these will be made.
                              
    \item [$\bullet$] The proposed framework will integrate the training and 
                      implementation process and will take as inputs a high level
                      description of the neural network and the hardware platform
                      specifications. Also, the framework will allow to use the software 
                      libraries available, explore the design space, and will propose
                      the most cost-effective hardware implementation, according to 
                      the processing flow presented in section  \ref{sec:hnn_fwk}.
    
    \item [$\bullet$] To evaluate the proposed framework the metrics presented in 
                      section \ref{subsec:fwk_met} and \ref{subsubsec:fer_stg} will 
                      be used. The evaluation results will be compared with the ones 
                      obtained from the NN implementation by means of the  software 
                      libraries. 
  \end{itemize}

   
\section{Expected Results}
     
The results of the development process and their expected time (E.T.) are presented in 
in Table \ref{tab:exp_res}.
{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table}[!htbp] 
  \begin{center}
  \begin{tabular}{p{3cm} p{6cm} p{2.0cm}}\hline
  % use packages: color,colortbl
  \rowcolor{tcA}
  \textbf{Result}	           & \textbf{Indicator}                         & \textbf{E.T. (semesters)}\\\hline
  {CNN models comparison}          & Study and comparison of at least two 
                                     CNN models with different layers types.
                                     (e.g. ReLU, Max-Pooling, etc).             & \cn S4-S5 \tn\hline
  {NN software implementation}     & Implementation of CNN models in software.  & \cn S4-S5 \tn\hline
  {FER application implementation} & Implementation of a FER application by
                                     using software libraries.                  & \cn S4-S5 \tn\hline
  {NN accelerator implementation}  & Implementation of CNN accelerator.         & \cn    S5 \tn\hline
  {NN hw-sw co-design}             & Design and implementation of a hw-sw 
                                     architecture for training the CNN model.   & \cn S5-S7 \tn\hline
  {HNN framework}                  & Design a new framework for training CNN
                                     in hardware.                               & \cn S5-S7 \tn\hline       
  {Publication}                    & Publication of the results in 
                                     conferences or journals.                   & \cn S4-S8 \tn\hline
  {Project Documentation}          & Documentation of the project
                                     development including hardware and
                                     software implementations and 
                                     the proposed framework.                    & \cn S4-S8 \tn\hline
  \end{tabular}
  \end{center}
  \vspace{-0.5cm}
  \caption[Indicators and expected time in semesters for the project results.]{Indicators and expected time in semesters for the project results.}
  \label{tab:exp_res}.
\end{table}
}


\section{Expected Impacts}
  
The research project will have the following impacts:
 
\subsection{Scientific and Technological Impacts}
  \begin{itemize}
                      
    \item [$\bullet$] Development of a framework that allows to implement neural 
                      networks in hardware.
                      
    \item [$\bullet$] Documentation of the developed work, especially the hardware 
                      neural network framework.
                      
    \item [$\bullet$] Publication of the results in conferences and journals such 
                      as IEEE COLCACI 2018, Neural Networks from Elsevier and 
                      the IEEE Transactions on Neural Networks and Learning Systems.
                      
  \end{itemize}


\subsection{Impacts over the productivity and the competitiveness in the productive sector}
  \begin{itemize}
    \item [$\bullet$] Make easier the process of designing, training and  implementing 
                      neural networks in hardware for FER applications.
                      
    \item [$\bullet$] Improve the viability of using low cost reconfigurable platforms 
                      for implementing hardware neural networks.
                      
    \item [$\bullet$] Conceive tools, such as  frameworks, that contribute to the 
                      technological development.
  \end{itemize}

\subsection{Environment and society impacts}
  \begin{itemize}
    \item [$\bullet$] Decrease  the platform power consumption due to the use of 
                      smaller FPGAs.
                      
    \item [$\bullet$] The implementation of hardware neural networks can lead to 
                      the integration of small FPGA platforms or embedded systems, 
                      that can be integrated to the industry and can be the core 
                      of some control devices.
  \end{itemize}



  
  
\section{Research Methodology}

The methodology that is going to be used in the  framework development (show in \ref{fig:metho})
is based on the Conceive, Design, Implement and Operate (CDIO) approach, created by the 
Massachusetts Institute of Technology (MIT) in the late 1990s, and which
is shown in Figure \ref{fig:cdio} \cite{CDIO} \cite{CDIO2014}.

\begin{figure}[!htbp]
  \centering
  \begin{tikzpicture}[
    auto,
    process/.style  = { rectangle, draw=blue, thick, fill=blue!10, 
                        text width=7em, text centered,
                        minimum height=4em, rounded corners }
    ]
    
    % Define nodes in a matrix
    \matrix [column sep=5mm, row sep=10mm] {
             & \node [process]  (conceive)  {Conceive};  & 
             & \node [process]  (design)    {Design};    & 
             & \node [process]  (implement) {Implement}; & 
             & \node [process]  (operate)   {Operate};   & \\
    };
    
    % connect all nodes defined above
    \draw[->,draw=black] (conceive)  to (design);
    \draw[->,draw=black] (design)    to (implement);
    \draw[->,draw=black] (implement) to (operate);
    
  \end{tikzpicture}
  \caption{CDIO Initiative taken from \cite{CDIO} \cite{CDIO2014}.}
  \label{fig:cdio}
\end{figure}




\begin{figure}[!htbp]
  \centering
  \begin{tikzpicture}[
    auto,
    start/.style    = { circle, draw=blue, thick, fill=blue!10, 
                        text width=2em, text centered,
                        minimum height=2em, rounded corners },
    input/.style    = { trapezium, draw=blue, thick, fill=blue!10, 
                        text width=6em, text centered,
                        minimum height=3em, rounded corners,
                        trapezium left angle=70, trapezium right angle=110},
    process/.style  = { rectangle, draw=blue, thick, fill=blue!10, 
                        text width=10em, text centered,
                        minimum height=3em, rounded corners },
    decision/.style = { diamond, draw=blue, thick, fill=blue!10, 
                        text width=6em, text centered,
                        minimum height=3em, rounded corners }                        
    ]
    
    % Define nodes in a matrix
    \matrix [column sep=5mm, row sep=10mm] {
             & \node            [process] (soa)  {State of the art review};                     & \\
             & \node            [process] (cnn)  {Study the CNN models};                        & \\
             & \node at (-4, 0) [process] (lib)  {Identify the limitations of sw libraries};    & \\
             & \node at ( 4, 0) [process] (pla)  {Identify the limitations of hw platforms};    & \\
             & \node            [process] (acc)  {Design an accelerator};                       & \\ 
             & \node            [process] (tra)  {Hw-sw architecture to train the CNN model};   & \\
             & \node            [process] (val)  {Has the architecture fill out the requirements?};& \\
             & \node at ( 4, 0) [process] (fra)  {Integrate into a framework};                  & \\
             & \node at ( 4, 0) [process] (vfr)  {Validate the framework};                      & \\
             & \node at ( 4, 0) [process] (doc)  {Documentation and publication process};       & \\
    };
    
    % connect all nodes defined above
    \draw[->,draw=black] (soa)  to (cnn);
    \draw[->,draw=black] (cnn)  |- (lib);
    \draw[->,draw=black] (cnn)  |- (pla);
    \draw[->,draw=black] (lib)  |- (acc);
    \draw[->,draw=black] (pla)  |- (acc);
    \draw[->,draw=black] (acc)  to (tra);
    \draw[->,draw=black] (tra)  to (val);
    \draw[->,draw=black] (val)  -| (-6,-2) |- (tra);
    \draw[->,draw=black] (val)  |- (fra);
    \draw[->,draw=black] (fra)  to (vfr);
    \draw[->,draw=black] (vfr)  to (doc);
        
  \end{tikzpicture}
  \caption{Research Project Methodology.}
  \label{fig:metho}
\end{figure}


As can be seen from Figure \ref{fig:metho}, the first step is to make a literature
review oriented to identify the limitations of the proposed works about HNN 
frameworks. Then, a study of the current CNN models used in FER systems is needed
to determine the parameters to which the software implementation is most sensible
to, and  compare  the software libraries available  identifying their limitations 
and strengths. 
Furthermore, review the most known hardware-software platforms and recognize their 
restrictions oriented to FER applications. Hence, the next step is to implement a 
CNN model to evaluate the software libraries available and extract the requirements 
needed to design and create an accelerator  to evaluate the trained model.
What follows now is to design and implement the training algorithm into a 
hardware-software architecture, which will be evaluated, validated and fitted
until it accomplish an acceptable performance. Here, the decision about where to
place the design will be essential.
Finally, the above-mentioned steps will be integrated into a new framework that take
the CNN model description, generate the files needed to implement the NN training process
in the hardware-software platform, and report the number of resources needed, 
the expected performance and give advice about how to improve the CNN model, the 
accuracy, and how to distribute the design to fit the platform.


\section{Timeline}
Tables \ref{tab:proj_tsk} and \ref{tab:time_line} show tasks involved in the project 
development and the Expected Time (E.T.) in semesters line for them, as can be seen,
the project starts in January 2017 and ends in June 2020.



%-- activity table
\begin{table}[!htbp]
  \definecolor{tcA}{rgb}{0.862745,0.862745,0.862745}
  \begin{center}
    \begin{tabular}{|p{0.5cm}|p{4.0cm}|p{7.0cm}|p{0.6cm}|}\hline
      % use packages: color,colortbl
      \rowcolor{tcA} 
      \textbf{N.} & \textbf{Stage}                     & \textbf{Indicator}                          & \textbf{E.T.}\\\hline
              {1} & {Literature revision}              & {Publication of the literature review 
                                                          in a conference.}                          & {1} \\\hline
              {2} & {Implementing a FER application}   & {Software implementation of a basic FER
                                                          application with at least 2 libraries.}    & {1} \\\hline                                                          
              {3} & {Select the NN topology}           & {Comparison of different CNN models.}       & {1} \\\hline
              {4} & {Select the NN software library}   & {Comparison of different CNN libraries.}    & {1} \\\hline
              {5} & {HNN accelerator}                  & {Implementation of a hardware accelerator.} & {2} \\\hline
              {6} & {Training Co-design}               & {Design and implementation of a hw-sw 
                                                          co-design for implementing a NN.}          & {2} \\\hline
              {7} & {Framework integration}            & {Integration of the available libraries 
                                                          with the co-design into the framework.}    & {2} \\\hline
              {8} & {Framework interface}              & {User interface design.}                    & {1} \\\hline
              {9} & {Framework evaluation}             & {Comparison between literature reported  
                                                          metrics with known databases.}             & {1} \\\hline
             {10} & {Documentation}                    & {Thesis reporting the development process.} & {1} \\\hline
             {11} & {Publication}                      & {Publication in conferences, journals, etc.}& {1} \\\hline
    \end{tabular}
  \end{center}
  \caption[Project tasks]{Project tasks, indicator and expected time in semesters.}
  \label{tab:proj_tsk}
\end{table}



%-- Time line table
{
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tn}{\tabularnewline}
\newcommand{\ti}{\textit}
\newcommand{\tb}{\textbf}
\definecolor{tcB}{rgb}{0.752941,0.8509,1} % blue :-)
\definecolor{tcR}{rgb}{1       ,0     ,0} % Red
\definecolor{tcO}{rgb}{1       ,0.5843,0} % Orange
\definecolor{tcY}{rgb}{1       ,1     ,0} % Yellow
\definecolor{tcGY}{rgb}{0.8235 ,1     ,0.0274} % Green-Yellow
\definecolor{tcG}{rgb}{0       ,1      ,0} % Green
\definecolor{tcW}{rgb}{1       ,1     ,1} % White
    
\begin{table}[!htbp]
  \begin{center}
    \hspace{-1.0cm} 2017  \hspace{7cm} 2020\\  
    %\begin{tabular}{|l|l|l|l|l|l|l|l|}\hline
    \begin{tabular}{|P{1.0cm}|P{1.0cm}|P{1.0cm}|P{1.0cm}|P{1.0cm}|P{1.0cm}|P{1.0cm}|P{1.0cm}|}\hline
      \rowcolor{tcB}                                                                                                     
      \tb{Task} & \tb{S2}  &\tb{S3}   &\tb{S4}   &\tb{S5}   &\tb{S6}   &\tb{S7}   &\tb{S8}  \\\hline
            {1} &\cc{tcG}  &\cc{tcG}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW} \\\hline
            {2} &\cc{tcG}  &\cc{tcG}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW} \\\hline
            {3} &\cc{tcG}  &\cc{tcG}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW} \\\hline
            {4} &\cc{tcW}  &\cc{tcGY} &\cc{tcGY} &\cc{tcGY} &\cc{tcGY} &\cc{tcW}  &\cc{tcW} \\\hline
            {5} &\cc{tcW}  &\cc{tcGY} &\cc{tcGY} &\cc{tcGY} &\cc{tcGY} &\cc{tcW}  &\cc{tcW} \\\hline
            {6} &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcY}  &\cc{tcY}  &\cc{tcY}  &\cc{tcW} \\\hline
            {7} &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcY}  &\cc{tcY}  &\cc{tcW}  &\cc{tcW} \\\hline
            {8} &\cc{tcW}  &\cc{tcW}  &\cc{tcO}  &\cc{tcO}  &\cc{tcO}  &\cc{tcO}  &\cc{tcO} \\\hline
            {9} &\cc{tcW}  &\cc{tcW}  &\cc{tcO}  &\cc{tcO}  &\cc{tcO}  &\cc{tcO}  &\cc{tcO} \\\hline
           {10} &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcR}  &\cc{tcR}  &\cc{tcR} \\\hline
           {11} &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcW}  &\cc{tcR}  &\cc{tcR}  &\cc{tcR} \\\hline
    \end{tabular}
  \end{center}
  \caption[Time Line]{Time Line.}
  \label{tab:time_line}
\end{table}
}
  
  
   

\section{Project Budget Management}

Detailed information about the budget needed to the development of this project, such 
as human resources budget, hardware platform budget, etc, is presented in this section.
  
\subsection{Total budget}
{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table}[!htbp] 
  \begin{center}
  \begin{tabular}{p{3cm} p{2cm}}\hline
  % use packages: color,colortbl
  \rowcolor{tcA}
  \textbf{\cn Item}	           & \textbf{ \cn Total}\\\hline
  {Human Resources}                & \cn {$\$  70'000.000$}\tn\hline
  {Hardware platform and software} & \cn {$\$   7'000.000$}\tn\hline
  {Results Exposure}               & \cn {$\$  22'500.000$}\tn\hline
  {TOTAL}                          & \cn {$\$  99'500.000$}\tn\hline
  \end{tabular}
  \end{center}
  \vspace{-0.5cm}
  \caption[Total Budget]{Total Budget.}
\end{table}
}


\subsection{Human Resources}
{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table}[!htbp] 
  \begin{center}
  \begin{tabular}{p{3cm} p{2cm} p{2cm}}\hline
  % use packages: color,colortbl
  \rowcolor{tcA}
  \textbf{Profession}  & \textbf{Function}  & \textbf{Total}\\\hline
  {PhD. Student}       & Researcher         & \cn {$\$  20'000.000$}\tn\hline
  {PhD. Professor}     & Advisor            & \cn {$\$  50'000.000$}\tn\hline
  {}                   & {TOTAL}            & \cn {$\$  70'000.000$}\tn\hline
  \end{tabular}
  \end{center}
  \vspace{-0.5cm}
  \caption[Human Resources Budget]{Human Resources Budget.}
\end{table}
}

\vspace{2.0cm}

\subsection{Hardware Platform and Software}

{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table}[!htbp] 
  \begin{center}
  \begin{tabular}{p{1cm} p{2cm} p{4cm} p{2cm}}\hline
  % use packages: color,colortbl
  \rowcolor{tcA}
  \textbf{Quant.} & \textbf{Item}       & \textbf{Description}                        & \textbf{Total}\\\hline
  \cn {1}         & Personal Computer   & Documentation and NN implementation.        & \cn {$\$   2'000.000$}\tn\hline
  \cn {1}         & NN library          & NN libraries such as TensorFlow and Theano. & \cn {$\$           0$}\tn\hline
  \cn {2}         & Hardware platforma  & Hardware platform for implementing NN.      & \cn {$\$   5'000.000$}\tn\hline
  {}              & {}                  & {TOTAL}                                     & \cn {$\$   7'000.000$}\tn\hline
  \end{tabular}
  \end{center}
  \vspace{-0.5cm}
  \caption[Hardware Platform and Software]{Hardware Platform and Software.}
\end{table}
}
    

    
\subsection{Results Exposure}

{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table}[!htbp] 
  \begin{center}
  \begin{tabular}{p{5cm} p{2cm}}\hline
  % use packages: color,colortbl
  \rowcolor{tcA}
  \textbf{Description}                & \textbf{Total}\\\hline
  Conferences and symposiums          & \cn {$\$  15'000.000$}\tn\hline
  Stationery                          & \cn {$\$   1'500.000$}\tn\hline
  Databases and printed documentation & \cn {$\$   6'000.000$}\tn\hline
  {TOTAL}                             & \cn {$\$  22'500.000$}\tn\hline
  \end{tabular}
  \end{center}
  \vspace{-0.5cm}
  \caption[Hardware Platform and Software]{Hardware Platform and Software.}
\end{table}
}    


\subsection{Financial Support}
This work would be supported by using the resources available at the Universidad 
Nacional de Colombia.