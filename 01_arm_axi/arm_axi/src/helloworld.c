/*
 * helloworld.c: simple test application
 *
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
#include <stdio.h>
#include "platform.h"
#include "xparameters.h"// Information about AXI peripherals
#include "xgpiops.h"    // Processing system GPIO driver
#include "xgpio.h"      // AXI GPIO driver
#include "xil_printf.h" // Data over UART
#include "xuartlite.h"  // Data over AXI UART


#define delay_time 20000000
#define MIO07  7
#define MIO50 50
#define MIO51 51


XGpioPs MIO_GPIO;   // Structure to handle the PS  GPIO
XGpio   AXI_GPIO0;  // Structure to handle the AXI GPIO 0
XGpio   AXI_GPIO1;  // Structure to handle the AXI GPIO 1
XUartLite AXI_UartLite; // Structure to handle the AXI UART 0


// Delay function
//-----------------------------
void delay(int time){
  int i;
  for (i = 0; i < time; i++);};


//-----------------------------
// Main function
//-----------------------------
int main()
{


	int StatusPS, StatusUart;
	u32 bou0, bou1;
	u32 pl_sws, pl_bou;
	u8  buffer[8] = "Salut!";

	XGpioPs_Config *GPIOConfigPtrPS;   // Pointer to the driver structure
    XGpio_Initialize(&AXI_GPIO0, 0);   // Initialize AXI GPIO 0
    XGpio_Initialize(&AXI_GPIO1, 1);   // Initialize AXI GPIO 1


	init_platform();

	// UartLite initialization
	StatusUart = XUartLite_Initialize(&AXI_UartLite, XPAR_AXI_UARTLITE_0_DEVICE_ID);
	if (StatusUart != XST_SUCCESS){
		print("UartLite INIT FAILED\n\r");
        return XST_FAILURE; }

	// GPIO PS initialization
	GPIOConfigPtrPS = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID); // init pointer
	StatusPS = XGpioPs_CfgInitialize(&MIO_GPIO, GPIOConfigPtrPS, GPIOConfigPtrPS -> BaseAddr);
    if (StatusPS != XST_SUCCESS){
        print("GPIO INIT FAILED\n\r");
        return XST_FAILURE; };

	// Configurer led MIO 07
    XGpioPs_SetDirectionPin(&MIO_GPIO,    MIO07, 0x1);
    XGpioPs_SetOutputEnablePin(&MIO_GPIO, MIO07, 0x1);

	// Configurer bouton 0 -> MIO 50
    XGpioPs_SetDirectionPin(&MIO_GPIO, MIO50, 0x0);

	// Configurer bouton 1 -> MIO 51
    XGpioPs_SetDirectionPin(&MIO_GPIO, MIO51, 0x0);

    // set AXI GPIO 0 channel (1, 2) tristates to Output (0) or inputs (1)
    XGpio_SetDataDirection(&AXI_GPIO0, 1, 0x00000000); // leds
    XGpio_SetDataDirection(&AXI_GPIO0, 2, 0x0000000F); // sws

    // set AXI GPIO 1 channel (1, 2) tristates to Output (0) or inputs (1)
    XGpio_SetDataDirection(&AXI_GPIO1, 1, 0x00000000); // rgbs
    XGpio_SetDataDirection(&AXI_GPIO1, 2, 0x0000000F); // pushs

    print("Bienvenue a cette logiciel \n\r");
    print("Par Dorfell Parra          \n\r");
    print("2019 03 12                 \n\r");
    print("---------------------------\n\r");
    print(" Faire allumer un led avec \n\r");
    print(" les boutons et tester le  \n\r");
    print(" AXI GPIO                  \n\r");
    print("---------------------------\n\r");


    while(1){

    	//----- PS GPIO ----
    	bou0 = XGpioPs_ReadPin(&MIO_GPIO, MIO50);
    	bou1 = XGpioPs_ReadPin(&MIO_GPIO, MIO51);

    	printf("valeur de bou0: %lu. \n\r", bou0);
    	printf("valeur de bou1: %lu. \n\r", bou1);

    	// Déclaration d' une porte logique OR
    	if ( bou1 == 0 && bou0 == 0){
		    XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x0);}   // Éteindre PS led
            //print("Eteindre le  MIO07 led... \n\r");}
    	else if ( bou1 == 0 && bou0 == 1){
		    XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x1);}   // Éteindre PS led
		    //print("Allumer le  MIO07 led... \n\r");}
    	else if ( bou1 == 1 && bou0 == 0){
		    XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x1);}   // Éteindre PS led
		    //print("Allumer le  MIO07 led... \n\r");}
    	else if ( bou1 == 1 && bou0 == 1){
		    XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x1);}   // Éteindre PS led
		    //print("Allumer le MIO07 led... \n\r");}
	    else{
    	    print("Ce n'est pas une option... \n\r");}

    	//----- AXI GPIO ----
    	pl_sws = XGpio_DiscreteRead(&AXI_GPIO0, 2);     // Lire les interrupteurs
    	pl_bou = XGpio_DiscreteRead(&AXI_GPIO1, 2);     // Lire les boutons

    	printf("valeur de pl_sws: %lu. \n\r", pl_sws);
    	printf("valeur de pl_bou << 3 : %lu. \n\r", pl_bou << 3);
    	printf("valeur de pl_bou: %lu. \n\r", pl_bou);

    	// Allumer leds avec les interrupteurs
    	XGpio_DiscreteWrite(&AXI_GPIO0, 1, pl_sws);

    	// Allumer les rgbs avec les boutons
    	XGpio_DiscreteWrite(&AXI_GPIO1, 1, (pl_bou + (pl_bou << 3)));  // Rouge

    	//----- AXI UART ----
    	XUartLite_Send(&AXI_UartLite, buffer, 8);

    	delay(delay_time);

    };

    return 0;
}
