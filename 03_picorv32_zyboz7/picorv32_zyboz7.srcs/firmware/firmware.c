/******************************/
/* Tester les operations      */ 
/* arithmétiques.             */
/******************************/

void put_int(int result){
    // L'entier est passé à outbyte.
    *(volatile int*)0x10000000 = result;
}

/* Déclaration des variables */
int a = 2; int b = 3; int c=0; // Pour l'addition        c = a + b
int d = 3; int e = 2; int f=0; // Pour la soustration    f = d - e
int g = 2; int h = 3; int i=0; // Pour la multiplication i = g * h
int j = 4; int k = 2; int l=0; // Pour la division       l = j / k

void main(){

    c = a + b;  put_int(c);
    f = d - e;  put_int(f);
    i = g * h;  put_int(i);
    l = j / k;  put_int(l);

}
