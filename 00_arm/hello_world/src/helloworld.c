/*
 * helloworld.c: simple test application
 *
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xgpiops.h"
#include "xil_printf.h"


#define delay_time 20000000
#define MIO07  7
#define MIO50 50
#define MIO51 51


XGpioPs MIO_GPIO;   // Structure to handle the gpio


// Delay function
//-----------------------------
void delay(int time){
  int i;
  for (i = 0; i < time; i++);};


//-----------------------------
// Main function
//-----------------------------
int main()
{


	int Status;
	u32 bou0, bou1;

	XGpioPs_Config *GPIOConfigPtrPS;           // Pointer to the driver structure

	init_platform();


	GPIOConfigPtrPS = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID); // init pointer
	Status = XGpioPs_CfgInitialize(&MIO_GPIO, GPIOConfigPtrPS, GPIOConfigPtrPS -> BaseAddr);
    if (Status != XST_SUCCESS){
        print("GPIO INIT FAILED\n\r");
        return XST_FAILURE; };

	// Configurer led MIO 07
    XGpioPs_SetDirectionPin(&MIO_GPIO, MIO07, 0x1);
    XGpioPs_SetOutputEnablePin(&MIO_GPIO, MIO07, 0x1);

	// Configurer bouton 0 -> MIO 50
    XGpioPs_SetDirectionPin(&MIO_GPIO, MIO50, 0x0);

	// Configurer bouton 1 -> MIO 51
    XGpioPs_SetDirectionPin(&MIO_GPIO, MIO51, 0x0);


    print("Bienvenue a cette logiciel \n\r");
    print("Par Dorfell Parra          \n\r");
    print("2019 12 10                 \n\r");
    print("---------------------------\n\r");
    print(" Faire allumer un led avec \n\r");
    print(" les boutons.              \n\r");
    print("---------------------------\n\r");


    while(1){

    	bou0 = XGpioPs_ReadPin(&MIO_GPIO, MIO50);
    	bou1 = XGpioPs_ReadPin(&MIO_GPIO, MIO51);

    	printf("valeur de bou0: %lu. \n\r", bou0);
    	printf("valeur de bou1: %lu. \n\r", bou1);

    	// Déclaration d' une porte logique OR
    	if ( bou1 == 0 && bou0 == 0){
		    // Eteindre led
		    XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x0);
		    print("Eteindre le  MIO07 led... \n\r");}
    	else if ( bou1 == 0 && bou0 == 1){
    	    // Eteindre led
    	    XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x0);
    	    print("Eteindre le  MIO07 led... \n\r");}
    	else if ( bou1 == 1 && bou0 == 0){
    	    // Eteindre led
    	    XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x0);
    	    print("Eteindre le  MIO07 led... \n\r");}
    	else if ( bou1 == 1 && bou0 == 1){
    	    // Allumer led
	    	XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x1);
	        print("Allumer le MIO07 led... \n\r");}
	    else{
    	    print("Ce n'est pas une option... \n\r");}


    	delay(delay_time);
    };

    return 0;
}
