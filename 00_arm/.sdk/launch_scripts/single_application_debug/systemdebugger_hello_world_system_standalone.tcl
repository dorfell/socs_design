connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zybo Z7 210351A81D30A" && level==0} -index 1
fpga -file /home/dorfell/Documents/Vitis_2019.2/socs_design/00_arm/hello_world/_ide/bitstream/arm_bd_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /home/dorfell/Documents/Vitis_2019.2/socs_design/00_arm/arm_bd_wrapper/export/arm_bd_wrapper/hw/arm_bd_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /home/dorfell/Documents/Vitis_2019.2/socs_design/00_arm/hello_world/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /home/dorfell/Documents/Vitis_2019.2/socs_design/00_arm/hello_world/Debug/hello_world.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con
